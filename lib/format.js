const fiat = new Intl.NumberFormat("en", {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 2,
});

const btc = new Intl.NumberFormat("en", {
  style: "currency",
  currency: "XBT",
  minimumFractionDigits: 8,
});

const sats = new Intl.NumberFormat("es", {
  style: "currency",
  currency: "XBT",
  minimumFractionDigits: 0,
});

export const formatBtc = ({ amount, currency }) => {
  if (amount > 1e8) {
    return btc.format(amount).replace("XBT", "₿");
  } else {
    return sats.format(Number(amount) * 1e8).replace("XBT", "sats");
  }
};

export const formatAmount = ({ amount, currency }) => {
  if (currency === "BTC") {
    return formatBtc({ amount, currency });
  } else {
    return fiat.format(amount);
  }
};

import { createClient } from "redis";

const client = createClient({
  url: process.env.REDIS_URL,
});

client.on("error", (err) => console.error(err));
client.connect();

const serialize = (value) => JSON.stringify(value);
const deserialize = (str) => JSON.parse(str);

async function set(key, value, expiry) {
  return client.set(
    key,
    serialize(value),
    expiry
      ? {
          EX: expiry,
          NX: true,
        }
      : { NX: true }
  );
}

async function get(key) {
  const value = await client.get(key);
  return value ? deserialize(value) : value;
}

export default { client, get, set };

import axios from "axios";

import redis from "./redis";

const STRIKE_API_URL = process.env.STRIKE_API_URL;
const STRIKE_API_TOKEN = process.env.STRIKE_API_TOKEN;
const WEBHOOK_SECRET = process.env.WEBHOOK_SECRET;

const client = axios.create({
  baseURL: STRIKE_API_URL,
  timeout: 5000,
  headers: {
    Authorization: `Bearer ${STRIKE_API_TOKEN}`,
    "Content-Type": "application/json",
  },
});

export const getProfileById = async (id) => {
  try {
    const { data } = await client.get(`/accounts/${id}/profile`);
    await redis.set(`users:${data.handle}`, data, 60 * 60);
    return data;
  } catch (error) {
    return null;
  }
};

export const getProfileByHandle = async (handle) => {
  const maybeUser = await redis.get(`users:${handle}`);
  if (maybeUser) {
    return maybeUser === "NOT_FOUND" ? null : maybeUser;
  }

  try {
    const { data } = await client.get(`/accounts/handle/${handle}/profile`);
    await redis.set(`users:${handle}`, data, 60 * 60);
    return data;
  } catch (error) {
    await redis.set(`users:${handle}`, "NOT_FOUND");
    return null;
  }
};

export const getRates = async () => {
  const { data } = await client.get(`/rates/ticker`);
  return data;
};

export const getBtcRateInSats = async (currency) => {
  const rates = await getRates();
  const btcRate = rates.find(
    ({ sourceCurrency, targetCurrency }) =>
      sourceCurrency === currency && targetCurrency === "BTC"
  );
  return Number(btcRate.amount) * 1e8;
};

export const createInvoiceForHandle = async (
  handle,
  { correlationId, amount, currency, description }
) => {
  const { data } = await client.post(`/invoices/handle/${handle}`, {
    amount: { amount: amount, currency },
    description,
    correlationId,
  });
  return data;
};

export const getInvoice = async (id) => {
  const { data } = await client.get(`/invoices/${id}`);
  return data;
};

export const createQuote = async (id, descriptionHash) => {
  const url = descriptionHash
    ? `/invoices/${id}/quote?descriptionHash=${descriptionHash}`
    : `/invoices/${id}/quote`;
  const { data } = await client.post(url);
  return data;
};

export const createSubscription = async (url) => {
  const { data } = await client.post(`/subscriptions`, {
    webhookUrl: url,
    webhookVersion: "v1",
    secret: WEBHOOK_SECRET,
    enabled: true,
    eventTypes: ["invoice.updated"],
  });
  return data;
};

export const getEvents = async () => {
  const { data } = await client.get("/events");
  return data;
};

export const getFilteredEvents = async ({ eventType }) => {
  const { data } = await client.get(
    `/events?$filter=eventType%20eq%20%27${eventType}%27`
  );
  return data;
};

import { defineMessages } from "react-intl";

export default defineMessages({
  title: {
    id: "title",
    defaultMessage: "A cash register for Strike users",
  },
  log_in: {
    id: "log_in",
    defaultMessage: "Log in with Strike",
  },
  log_out: {
    id: "log_out",
    defaultMessage: "Log out",
  },
  home_title: {
    id: "home_title",
    defaultMessage: "A cash register for {strikeLink} users",
  },
  home_enter_username: {
    id: "home_enter_username",
    defaultMessage: "Enter your username",
  },
  home_go_button: {
    id: "home_go_button",
    defaultMessage: "Press the arrow to go to your cash register",
  },
  footer: {
    id: "footer",
    defaultMessage: "Made with ❤️  by {author}",
  },
  pay_to: {
    id: "pay_to",
    defaultMessage: "Pay {handle}",
  },
  cash_charge: {
    id: "cash_charge",
    defaultMessage: "Charge",
  },
  open_in_wallet: {
    id: "open_in_wallet",
    defaultMessage: "⚡ Open in wallet",
  },
  qr_title: {
    id: "qr_title",
    defaultMessage: "QR for {handle}",
  },
  qr_scan: {
    id: "qr_scan",
    defaultMessage: "Scan this code to pay",
  },
  qr_ln: {
    id: "qr_ln",
    defaultMessage: "Or send to {lnAddress}",
  },
  copy: {
    id: "copy",
    defaultMessage: "Copy",
  },
  stats: {
    id: "stats",
    defaultMessage: "Amount transacted",
  },
});

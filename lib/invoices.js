import {
  createInvoiceForHandle as createStrikeInvoice,
  createQuote,
  getProfileByHandle,
} from "./api";
import {
  createInvoice as createDbInvoice,
  getInvoice as getDbInvoice,
  getQuote as getDbQuote,
  saveQuote,
} from "./db";
import { generateShortId } from "./id";

export async function createInvoice({ handle, amount, currency, description }) {
  const shortId = generateShortId();
  const { invoiceId } = await createStrikeInvoice(handle, {
    correlationId: shortId,
    amount,
    currency,
    description,
  });
  const invoice = await createDbInvoice({
    handle,
    shortId,
    invoiceId,
    amount,
    currency,
    description,
  });
  return invoice;
}

export async function getInvoice(invoiceShortId) {
  const invoice = await getDbInvoice(invoiceShortId);
  return invoice;
}

export async function getQuote(invoiceShortId, descriptionHash) {
  const maybeQuote = await getDbQuote(invoiceShortId);
  const now = Date.now();

  if (maybeQuote && Date.parse(maybeQuote.expiration) > now) {
    return maybeQuote;
  }

  const { invoiceId } = await getInvoice(invoiceShortId);
  const { quoteId, lnInvoice, onchainAddress, expiration, sourceAmount } =
    await createQuote(invoiceId, descriptionHash);
  const quote = await saveQuote(invoiceShortId, {
    quote: quoteId,
    ln: lnInvoice,
    onchain: onchainAddress,
    expiration,
    amount: sourceAmount?.amount,
  });

  return quote;
}

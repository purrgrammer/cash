import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient({
  rejectOnNotFound: false,
});

const readInvoice = ({
  short_id,
  invoice_id,
  created_at,
  paid,
  amount,
  ...rest
}) => {
  return {
    shortId: short_id,
    invoiceId: invoice_id,
    createdAt: +created_at,
    paid,
    amount: amount.toFixed(8),
    ...rest,
  };
};

export async function getInvoice(invoiceShortId) {
  return prisma.invoices
    .findUnique({
      where: { short_id: invoiceShortId },
    })
    .then((inv) => inv && readInvoice(inv));
}

export async function updateInvoice(invoiceShortId, data) {
  return prisma.invoices.update({
    where: { short_id: invoiceShortId },
    data,
  });
}

export async function createInvoice({
  handle,
  shortId,
  invoiceId,
  amount,
  currency,
  description,
}) {
  return prisma.invoices
    .create({
      data: {
        short_id: shortId,
        invoice_id: invoiceId,
        handle,
        amount,
        currency,
        description,
      },
    })
    .then(readInvoice);
}

export async function getQuote(invoiceShortId) {
  const maybeQuote = await prisma.quotes.findUnique({
    where: { invoice_short_id: invoiceShortId },
  });
  if (maybeQuote && maybeQuote.amount) {
    return { ...maybeQuote, amount: maybeQuote.amount.toFixed(8) };
  } else {
    return maybeQuote;
  }
}

export async function saveQuote(
  invoiceShortId,
  { quote, ln, onchain, expiration, amount }
) {
  return prisma.quotes.upsert({
    where: { invoice_short_id: invoiceShortId },
    update: { quote_id: quote, ln, onchain, expiration, amount },
    create: {
      invoice_short_id: invoiceShortId,
      quote_id: quote,
      ln,
      onchain,
      expiration,
      amount,
    },
  });
}

export async function getPaidInvoices(handle, { page } = { page: 1 }) {
  const where = {
    handle,
    paid: true,
  };

  const count = await prisma.invoices.count({ where });
  const invoices = await prisma.invoices
    .findMany({
      where,
      orderBy: {
        created_at: "desc",
      },
      skip: Math.max(0, page - 1) * 20,
      take: 20,
    })
    .then((invoices) => invoices.map(readInvoice));

  return { count, invoices };
}

export async function getTotalPaid(handle, filters = {}) {
  const where = {
    handle: handle,
    currency: {
      in: ["USD", "USDT"], // todo: EUR, GBP
    },
    paid: true,
    ...filters,
  };

  const invoices = await prisma.invoices.aggregate({
    where,
    _sum: {
      amount: true,
    },
    _count: {
      amount: true,
    },
  });

  return {
    txs: invoices._count.amount,
    total: invoices._sum.amount?.toNumber() || 0,
  };
}

export async function getTotalPaidInBtc(handle) {
  return getTotalPaid(handle, { currency: "BTC" });
}

export async function getTotalPaidByCurrency(filters = {}) {
  const where = {
    paid: true,
    ...filters,
  };

  const invoices = await prisma.invoices.groupBy({
    by: ["currency"],
    where,
    _sum: {
      amount: true,
    },
    _count: {
      amount: true,
    },
  });

  const counts = invoices.reduce((acc, stats) => {
    const totals = {
      total: stats._sum?.amount?.toNumber() || 0,
      txs: stats._count?.amount,
    };
    return { ...acc, [stats.currency]: totals };
  }, {});

  const usd = counts.USD || { total: 0, txs: 0 };
  const btc = counts.BTC || { total: 0, txs: 0 };

  return {
    USD: usd,
    BTC: btc,
  };
}

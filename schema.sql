CREATE EXTENSION "uuid-ossp";

CREATE TABLE invoices (
  -- Metadata
  id serial primary key,
  short_id VARCHAR(32) NOT NULL UNIQUE,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  -- Invoice fields
  handle VARCHAR(32) NOT NULL,
  description VARCHAR(250),
  invoice_id uuid NOT NULL,
  paid BOOLEAN NOT NULL DEFAULT FALSE,
  amount NUMERIC NOT NULL,
  currency VARCHAR(4) NOT NULL
);

CREATE TABLE quotes (
  -- Metadata
  id SERIAL PRIMARY KEY,
  quote_id UUID NOT NULL,
  invoice_short_id VARCHAR(32) UNIQUE NOT NULL REFERENCES invoices(short_id),
  -- Quote fields
  ln TEXT NOT NULL,
  onchain TEXT,
  amount NUMERIC,
  expiration TIMESTAMP NOT NULL
);

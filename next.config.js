/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  i18n: {
    locales: ["en", "es"],
    defaultLocale: "en",
    localeDetection: false,
  },
  async rewrites() {
    return [
      {
        source: "/.well-known/lnurlp/:path*",
        destination: "/api/lnurlpay/:path*",
      },
    ];
  },
};

module.exports = nextConfig;

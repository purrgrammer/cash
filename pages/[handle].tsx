import { useState, useEffect, useCallback } from "react";

import type { NextPage, GetServerSideProps } from "next";
import { useRouter } from "next/router";
import Head from "next/head";
import { requestProvider } from "webln";

import {
  ArrowBackIcon as BackIcon,
  CheckIcon,
  RepeatIcon,
  DeleteIcon,
  CopyIcon,
} from "@chakra-ui/icons";
import {
  useToken,
  useColorModeValue,
  useBreakpointValue,
  Container,
  Heading,
  Center,
  Button,
  VStack,
  Flex,
  Box,
  Spinner,
  Avatar,
  Link,
} from "@chakra-ui/react";
import { useIntl, FormattedMessage } from "react-intl";

import { getProfileByHandle } from "../lib/api";
import { useCopy } from "../lib/hooks";
import { AmountInput, CashRegister } from "../components/CashRegister";
import { Nav } from "../components/Nav";
import { QRCode } from "../components/QR";
import { QrIcon } from "../components/QrIcon";
import { DollarIcon } from "../components/DollarIcon";
import { Layout } from "../components/Layout";
import messages from "../lib/messages";

const Pay: NextPage<{
  handle: string;
  currency: string;
  user: any;
  desc?: string;
}> = ({ handle, currency, user, desc }) => {
  const intl = useIntl();
  const title = intl.formatMessage(messages.pay_to, { handle });
  const [amount, setAmount] = useState("0");
  const [description, setDescription] = useState(desc || "");
  const [invoice, setInvoice] = useState<any | null>();
  const [isFetchingInvoice, setIsFetchingInvoice] = useState(false);
  const { copy, copied } = useCopy();
  const sizes = [300, 400, 500];
  const width = useBreakpointValue(sizes);
  const [white, gray] = useToken("colors", ["white", "gray.800"]);
  const themeColor = useColorModeValue(white, gray);
  const router = useRouter();
  const buttonSize = useBreakpointValue([12, 20]);
  const boxSize = useBreakpointValue([5, 6]);
  const buttonMargin = 6;
  const [webln, setWebln] = useState(null);

  useEffect(() => {
    requestProvider()
      .then((provider) => {
        // @ts-expect-error
        setWebln(provider);
        return provider.enable();
      })
      .catch(() => {
        console.warn("WebLN provider not available");
      });
  }, []);

  const hasDefaultDescription = Boolean(desc);
  const isInvoiceAvailable = Boolean(invoice);
  const isCheckingOut = !(isFetchingInvoice || isInvoiceAvailable);
  const isInvoiceExpired =
    invoice?.expiration && new Date(invoice.expiration) < new Date();
  const isInvoicePaid = isInvoiceAvailable && invoice.paid;

  const clear = () => {
    setAmount("0");
    setInvoice(null);
  };

  const onAmountChange = (a: string) => {
    setAmount(a);
  };

  const createInvoice = () => {
    setIsFetchingInvoice(true);
    fetch("/api/invoices", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        handle,
        amount: {
          amount,
          currency,
        },
        description,
      }),
    })
      .then((r) => r.json())
      .catch((err) => {
        console.error(err);
      })
      .then((invoice) => {
        setIsFetchingInvoice(false);
        setInvoice(invoice);
        // @ts-expect-error
        webln?.sendPayment(invoice.ln).catch(() => {
          console.warn("WebLN sendPayment failed");
        });
      });
  };

  const updateInvoice = () => {
    fetch(`/api/invoices/${invoice.shortId}`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
      .then((r) => r.json())
      .catch((err) => {
        console.error(err);
      })
      .then((newInvoice) => {
        setInvoice({ ...invoice, ...newInvoice });
      });
  };

  const getInvoice = useCallback(() => {
    if (invoice && !isInvoiceExpired && !isInvoicePaid) {
      fetch(`/api/invoices/${invoice.shortId}`)
        .then((r) => r.json())
        .catch((err) => {
          console.error(err);
        })
        .then((newInvoice) => {
          setInvoice({ ...invoice, ...newInvoice });
        });
    }
  }, [invoice, isInvoiceExpired, isInvoicePaid]);

  useEffect(() => {
    const interval = setInterval(getInvoice, 1000);
    return () => {
      clearInterval(interval);
    };
  }, [getInvoice]);

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta property="og:title" content={handle} key="title" />
        <meta property="og:description" content={title} key="description" />
        {user.avatarUrl && (
          <meta property="og:image" content={user.avatarUrl} key="image" />
        )}
        <meta name="theme-color" content={themeColor} />
      </Head>
      <Layout>
        <Container height="100%" pt={4}>
          <Nav>
            <Button variant="unstyled" sx={{ cursor: "default" }}>
              <DollarIcon color="orange" />
            </Button>
            <Button
              variant="unstyled"
              onClick={() => router.push(`/qr/${handle}`)}
            >
              <QrIcon color="gray.300" />
            </Button>
          </Nav>
          <Center flexDirection="column">
            <Flex alignItems="center" flexDirection="column" mb={4}>
              <Avatar name={handle} src={user.avatarUrl} mr=".5em" size="xs" />
              <Heading fontSize="2xl">
                <Link href={`/${handle}`}>{handle}</Link>
              </Heading>
            </Flex>
            {isCheckingOut ? (
              <>
                <AmountInput amount={amount} currency={currency} />
                <Center
                  flexDirection="column"
                  height={sizes}
                  width={sizes}
                  my={0}
                >
                  <CashRegister
                    amount={amount}
                    onChange={onAmountChange}
                    sizes={sizes}
                  />
                </Center>
                <Flex
                  flexDirection="row"
                  alignItems="center"
                  justifyContent="space-between"
                  mt={buttonMargin}
                  width={width}
                >
                  <Button
                    onClick={clear}
                    variant="outline"
                    width={buttonSize}
                    height={buttonSize}
                  >
                    <DeleteIcon boxSize={boxSize} />
                  </Button>
                  <Button
                    disabled={Number(amount) === 0}
                    fontSize="2xl"
                    colorScheme="orange"
                    isLoading={isFetchingInvoice}
                    width="60%"
                    height={buttonSize}
                    onClick={createInvoice}
                  >
                    <FormattedMessage {...messages.cash_charge} />
                  </Button>
                </Flex>
              </>
            ) : (
              <>
                {!isInvoiceAvailable && (
                  <>
                    <AmountInput amount={amount} currency={currency} />
                    <Center height={sizes} width={sizes}>
                      <Spinner
                        color="orange"
                        emptyColor="gray.100"
                        thickness="8px"
                        boxSize={24}
                      />
                    </Center>
                    <Flex
                      flexDirection="row"
                      alignItems="center"
                      justifyContent="space-between"
                      width={width}
                      mt={buttonMargin}
                    >
                      <Button
                        disabled={true}
                        variant="outline"
                        height={buttonSize}
                      >
                        <BackIcon boxSize={boxSize} />
                      </Button>
                      <Button
                        disabled={true}
                        variant="outline"
                        height={buttonSize}
                      >
                        <RepeatIcon boxSize={boxSize} />
                      </Button>
                      <Button
                        disabled={true}
                        variant="outline"
                        height={buttonSize}
                      >
                        <CopyIcon boxSize={boxSize} />
                      </Button>
                    </Flex>
                  </>
                )}

                {isInvoiceAvailable && (
                  <>
                    <AmountInput amount={amount} currency={currency} />
                    <Center
                      flexDirection="column"
                      height={sizes}
                      width={sizes}
                      sx={{
                        filter:
                          isInvoiceExpired && !isInvoicePaid
                            ? "blur(4px)"
                            : "none",
                      }}
                      onClick={isInvoicePaid ? clear : () => {}}
                    >
                      {isInvoicePaid ? (
                        <>
                          <CheckIcon boxSize={40} color="green" />
                          <Heading color="green" size="4xl">
                            Paid
                          </Heading>
                        </>
                      ) : (
                        <QRCode
                          value={invoice.ln}
                          size={width}
                          onClick={() => !isInvoiceExpired && copy(invoice.ln)}
                        />
                      )}
                    </Center>
                    <Flex
                      flexDirection="row"
                      alignItems="center"
                      justifyContent="space-between"
                      width={width}
                      mt={buttonMargin}
                    >
                      <Button
                        onClick={clear}
                        height={buttonSize}
                        variant="outline"
                      >
                        <BackIcon boxSize={boxSize} />
                      </Button>
                      <Button
                        isLoading={isFetchingInvoice}
                        disabled={!isInvoiceExpired || isInvoicePaid}
                        height={buttonSize}
                        onClick={updateInvoice}
                        variant="outline"
                      >
                        <RepeatIcon boxSize={boxSize} />
                      </Button>
                      <Button
                        disabled={isInvoiceExpired || isInvoicePaid}
                        onClick={() => copy(invoice.ln)}
                        height={buttonSize}
                        variant="outline"
                      >
                        {copied ? (
                          <CheckIcon boxSize={boxSize} color="green" />
                        ) : (
                          <CopyIcon boxSize={boxSize} />
                        )}
                      </Button>
                    </Flex>
                    <Box mt={2} px={9} py={4}>
                      <Button
                        colorScheme="orange"
                        disabled={isInvoiceExpired || isInvoicePaid}
                        onClick={() =>
                          window.open(`lightning:${invoice.ln}`, "_blank")
                        }
                        width={sizes}
                      >
                        <FormattedMessage {...messages.open_in_wallet} />
                      </Button>
                    </Box>
                  </>
                )}
              </>
            )}
          </Center>
        </Container>
      </Layout>
    </>
  );
};

export default Pay;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { handle, desc } = context.query;

  try {
    const user = await getProfileByHandle(handle);

    if (user.canReceive) {
      const currency = user.currencies.find(
        ({ isDefaultCurrency }: any) => isDefaultCurrency
      ).currency;
      return { props: { handle, user, currency, desc: desc || null } };
    }

    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    };
  } catch (error) {
    console.error(error);
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    };
  }
};

import { useState, useEffect } from "react";

import type { AppProps } from "next/app";
import { useRouter } from "next/router";
import { SessionProvider } from "next-auth/react";

import { ChakraProvider } from "@chakra-ui/react";
import {
  createIntl,
  createIntlCache,
  RawIntlProvider,
  IntlProvider,
} from "react-intl";
import PlausibleProvider from "next-plausible";

import theme from "lib/theme";

import en from "translations/en.json";
import es from "translations/es.json";

const cache = createIntlCache();

const LocaleProvider = ({ children }: any) => {
  const { locale } = useRouter();
  const [intl, setIntl] = useState<any>(
    createIntl({ locale: "en", messages: en }, cache)
  );
  const [lang, setLang] = useState(locale);

  useEffect(() => {
    const [language] = window.navigator.language.toLowerCase().split(/[_-]+/);
    setIntl(
      createIntl(
        { locale: language, messages: language === "es" ? es : en },
        cache
      )
    );
  }, []);

  return <RawIntlProvider value={intl}>{children}</RawIntlProvider>;
};

export default function App({
  Component,
  pageProps: { session, ...pageProps },
}: AppProps) {
  return (
    <LocaleProvider>
      <SessionProvider>
        <ChakraProvider theme={theme}>
          <PlausibleProvider domain="strike.cash">
            <Component {...pageProps} />
          </PlausibleProvider>
        </ChakraProvider>
      </SessionProvider>
    </LocaleProvider>
  );
}

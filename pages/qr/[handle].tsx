import { useState, useEffect, useCallback } from "react";

import type { NextPage, GetServerSideProps } from "next";
import { useRouter } from "next/router";
import Head from "next/head";

import {
  ArrowBackIcon as BackIcon,
  CheckIcon,
  RepeatIcon,
  DeleteIcon,
  CopyIcon,
} from "@chakra-ui/icons";
import {
  useToken,
  useColorModeValue,
  useBreakpointValue,
  Container,
  Heading,
  Center,
  Button,
  VStack,
  Flex,
  Box,
  Spinner,
  Avatar,
  Text,
  Link,
} from "@chakra-ui/react";
import { useIntl, FormattedMessage } from "react-intl";
import { bech32 } from "bech32";

import { AmountInput, CashRegister } from "components/CashRegister";
import { Nav } from "components/Nav";
import { QRCode } from "components/QR";
import { QrIcon } from "components/QrIcon";
import { DollarIcon } from "components/DollarIcon";
import { Layout } from "components/Layout";

import { getProfileByHandle } from "lib/api";
import { useCopy } from "lib/hooks";
import messages from "lib/messages";

const QR: NextPage<{ handle: string; user: any; desc?: string }> = ({
  handle,
  user,
  desc,
}) => {
  const intl = useIntl();
  const title = intl.formatMessage(messages.qr_title, { handle });
  const router = useRouter();
  const { copy, copied } = useCopy();
  const sizes = [300, 400, 500];
  const width = useBreakpointValue(sizes);
  const [white, gray] = useToken("colors", ["white", "gray.800"]);
  const themeColor = useColorModeValue(white, gray);
  const words = Buffer.from(
    `https://strike.cash/api/lnurlpay/${handle}`,
    "utf-8"
  );
  const lnUrl = bech32.encode("LNURL", bech32.toWords(words));

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta property="og:title" content={handle} key="title" />
        <meta property="og:description" content={title} key="description" />
        {user.avatarUrl && (
          <meta property="og:image" content={user.avatarUrl} key="image" />
        )}
        <meta name="theme-color" content={themeColor} />
      </Head>
      <Layout>
        <Container height="100%" pt={4}>
          <Nav>
            <Button
              variant="unstyled"
              onClick={() => router.push(`/${handle}`)}
            >
              <DollarIcon color="gray.300" />
            </Button>
            <Button variant="unstyled" sx={{ cursor: "default" }}>
              <QrIcon color="orange" />
            </Button>
          </Nav>
          <Center flexDirection="column">
            <Flex alignItems="center" flexDirection="column" mb={4}>
              <Avatar name={handle} src={user.avatarUrl} mr=".5em" size="xs" />
              <Heading fontSize="2xl">
                <Link href={`/${handle}`}>{handle}</Link>
              </Heading>
              <Box width={sizes} textAlign="center" mt={2}>
                <Text fontSize="xl">
                  <FormattedMessage {...messages.qr_scan} />
                </Text>
                <Text color="gray.400">
                  <FormattedMessage
                    {...messages.qr_ln}
                    values={{
                      lnAddress: (
                        <Text as="span" fontWeight="bold" color="orange">
                          {handle}@strike.cash
                        </Text>
                      ),
                    }}
                  />
                </Text>
              </Box>
            </Flex>
            <Center flexDirection="column" height={sizes} width={sizes}>
              <QRCode value={lnUrl} size={width} onClick={() => copy(lnUrl)} />
            </Center>
            <Flex
              flexDirection="row"
              alignItems="center"
              justifyContent="space-between"
              width={width}
              mt={8}
            >
              <Button onClick={() => copy(lnUrl)} ml={2} variant="outline">
                {copied ? (
                  <CheckIcon color="green" mr="4px" />
                ) : (
                  <CopyIcon mr="4px" />
                )}
                <FormattedMessage {...messages.copy} />
              </Button>
              <Button
                colorScheme="orange"
                onClick={() => window.open(`lightning:${lnUrl}`, "_blank")}
              >
                <FormattedMessage {...messages.open_in_wallet} />
              </Button>
            </Flex>
          </Center>
        </Container>
      </Layout>
    </>
  );
};

export default QR;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { handle, desc } = context.query;

  try {
    const user = await getProfileByHandle(handle);

    if (user.canReceive) {
      return { props: { handle, user, desc: desc || null } };
    }

    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    };
  } catch (error) {
    console.error(error);
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    };
  }
};

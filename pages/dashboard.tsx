import { useState, useEffect } from "react";

import type { GetServerSideProps } from "next";

import Head from "next/head";
import {
  useToken,
  useColorModeValue,
  Flex,
  Container,
  Heading,
  Stat,
  StatLabel,
  StatNumber,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
} from "@chakra-ui/react";
import { getSession } from "next-auth/react";
import { useIntl, FormattedDate } from "react-intl";

import { getProfileById } from "lib/api";
import { getTotalPaidByCurrency } from "lib/db";
import { formatAmount } from "lib/format";
import { Nav } from "components/Nav";
import { Layout } from "components/Layout";

const Statistics = ({ stats, currency }: any) => {
  return (
    <Flex
      flexDirection={["column", "row"]}
      alignItems="center"
      justifyContent="space-around"
      width="100%"
    >
      <Flex
        flexDirection="column"
        alignItems="flex-start"
        justifyContent="space-around"
        width={["80%", "50%"]}
        mb={[12, 0]}
      >
        <Stat mb={4}>
          <StatLabel>{currency}</StatLabel>
          <StatNumber>
            {formatAmount({ amount: stats.USD.total, currency: "USD" })}
          </StatNumber>
        </Stat>
        <Stat>
          <StatLabel>{currency} txs</StatLabel>
          <StatNumber>{stats.USD.txs}</StatNumber>
        </Stat>
      </Flex>
      <Flex
        flexDirection="column"
        alignItems="flex-start"
        justifyContent="space-around"
        width={["80%", "50%"]}
      >
        <Stat mb={4}>
          <StatLabel>BTC</StatLabel>
          <StatNumber>₿{stats.BTC.total}</StatNumber>
        </Stat>

        <Stat>
          <StatLabel>BTC txs</StatLabel>
          <StatNumber>{stats.BTC.txs}</StatNumber>
        </Stat>
      </Flex>
    </Flex>
  );
};

const Invoices = ({ user, currency }: any) => {
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);
  const [isFetching, setIsFetching] = useState(false);
  const [invoices, setInvoices] = useState([]);

  useEffect(() => {
    setIsFetching(true);
    fetch("/api/dashboard/invoices")
      .then((r) => r.json())
      .then(({ count, invoices }) => {
        setCount(count);
        setInvoices(invoices);
        setIsFetching(false);
      });
  }, []);

  return (
    <TableContainer>
      <Table variant="simple">
        <Thead>
          <Tr>
            <Th>Description</Th>
            <Th>Amount</Th>
            <Th>Created</Th>
          </Tr>
        </Thead>
        <Tbody>
          {invoices.map(
            ({ shortId, amount, currency, description, createdAt }) => (
              <Tr key={shortId}>
                <Td>{description}</Td>
                <Td>{formatAmount({ amount, currency })}</Td>
                <Td>
                  <FormattedDate
                    value={new Date(createdAt)}
                    year="numeric"
                    month="short"
                    day="numeric"
                  />
                </Td>
              </Tr>
            )
          )}
        </Tbody>
      </Table>
    </TableContainer>
  );
};

const Dashboard = ({ user, currency, stats }: any) => {
  const [white, gray] = useToken("colors", ["white", "gray.800"]);
  const themeColor = useColorModeValue(white, gray);
  return (
    <>
      <Head>
        <title>{user.handle}</title>
        <meta name="theme-color" content={themeColor} />
      </Head>
      <Layout>
        <Nav showLogin showLogout loggedInUser={user} />
        <Flex
          flexDirection="column"
          alignItems="flex-start"
          width={"50%"}
          mx="auto"
          mt={20}
        >
          <Statistics currency={currency} stats={stats} />
          <Invoices user={user} currency={currency} />
        </Flex>
      </Layout>
    </>
  );
};

export default Dashboard;

export const getServerSideProps: GetServerSideProps = async (context) => {
  try {
    const session = await getSession(context);
    // @ts-expect-error
    const id = session?.user?.id;
    if (id) {
      const user = await getProfileById(id);
      const currency = user.currencies.find(
        ({ isDefaultCurrency }: any) => isDefaultCurrency
      ).currency;
      const stats = await getTotalPaidByCurrency({ handle: user.handle });
      return {
        props: {
          user,
          stats,
          currency,
        },
      };
    }

    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    };
  } catch (error) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    };
  }
};

import { useState, useEffect } from "react";

import type { NextPage, GetServerSideProps } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import { getSession } from "next-auth/react";
import { useIntl, FormattedMessage } from "react-intl";

import {
  useToken,
  useBreakpointValue,
  useColorModeValue,
  Flex,
  Container,
  Center,
  Heading,
  Avatar,
  InputGroup,
  Input,
  InputRightElement,
  FormControl,
  FormHelperText,
  Button,
  Text,
  Link,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Stat,
  StatLabel,
  StatNumber,
  StatHelpText,
} from "@chakra-ui/react";
import { ArrowForwardIcon, ExternalLinkIcon } from "@chakra-ui/icons";

import { Nav } from "components/Nav";
import { CashRegister, AmountInput } from "components/CashRegister";
import { Layout } from "components/Layout";
import { Footer } from "components/Footer";

import { getProfileById } from "lib/api";
import { getTotalPaidByCurrency } from "lib/db";
import messages from "lib/messages";
import { formatAmount } from "lib/format";

type FetchStatus = "FETCHING" | "FETCHED" | "ERROR";

const CashRegisterPreview = ({ user, ...rest }: any) => {
  const [amount, setAmount] = useState("21");
  const sizes = [300, 400, 500];

  return (
    <Container mx={0} py={4} {...rest} width={sizes}>
      <Flex alignItems="center" flexDirection="column">
        <Avatar
          name={user?.handle || "satoshi"}
          mr=".5em"
          size="xs"
          src={user?.avatarUrl}
        />
        <Heading fontSize="2xl">{user?.handle || "Satoshi"}</Heading>
        <AmountInput amount={amount} currency="USD" />
        <CashRegister amount={amount} onChange={setAmount} sizes={sizes} />
      </Flex>
    </Container>
  );
};

const Statistics = ({ total }: any) => {
  const currencies = Object.keys(total);
  const sizes = [300, 400, 500, 600];
  const [gray600, gray700] = useToken("colors", ["gray.600", "gray.700"]);
  const bg = useColorModeValue(gray600, gray700);
  return (
    <Flex
      alignItems="center"
      flexDirection={["column"]}
      width="100vw"
      color="white"
      bg={bg}
      py={8}
      mt={8}
    >
      <Heading
        textAlign={["center", "center", "left"]}
        size="3xl"
        mb={8}
        mx={4}
      >
        <FormattedMessage {...messages.stats} />
      </Heading>
      <Flex
        alignItems="center"
        flexDirection={["column", "column", "row"]}
        width={sizes}
        pl={4}
      >
        {currencies.map((currency) => {
          return (
            <Card
              ml={4}
              mr={4}
              mb={6}
              alignItems={["center", "center", "flex-start"]}
              width={["100%", "100%", "50%"]}
              height={"120px"}
              color="white"
              key={currency}
              bg={currency === "USD" ? "green.500" : "orange"}
            >
              <CardBody>
                <Stat>
                  <StatLabel>{currency}</StatLabel>
                  <StatNumber>
                    {formatAmount({
                      amount: total[currency].total,
                      currency,
                    })}
                  </StatNumber>
                  <StatHelpText>{total[currency].txs} txs</StatHelpText>
                </Stat>
              </CardBody>
            </Card>
          );
        })}
      </Flex>
    </Flex>
  );
};

const Home: NextPage<{ loggedInUser?: any; total?: any }> = ({
  loggedInUser,
  total,
}: any) => {
  const intl = useIntl();
  const title = intl.formatMessage(messages.title);
  const router = useRouter();
  const willShowStrikeLinkIcon = useBreakpointValue([false, true]);
  const strikeLink = (
    <Link href="https://strike.me/download" isExternal>
      Strike
      {willShowStrikeLinkIcon && (
        <ExternalLinkIcon color="gray.600" boxSize={5} ml="4px" />
      )}
    </Link>
  );
  const [white, gray] = useToken("colors", ["white", "gray.800"]);
  const themeColor = useColorModeValue(white, gray);
  const [username, setUsername] = useState("");
  const [users, setUsers] = useState<Record<string, [FetchStatus, any | null]>>(
    {}
  );
  const user = users[username] && users[username][1];
  const isFetchingUser = users[username] && users[username][0] === "FETCHING";
  const isFetchError = users[username] && users[username][0] === "ERROR";

  useEffect(() => {
    if (username.length >= 3) {
      const maybeUser = users[username];
      if (maybeUser) {
        return;
      }

      // @ts-expect-error
      setUsers({ ...users, [username]: ["FETCHING" as FetchStatus] });

      fetch(`/api/users/${username}`)
        .then((r) => r.json())
        .then((user) => {
          if (user.error) {
            // @ts-expect-error
            setUsers({ ...users, [username]: ["ERROR" as FetchStatus] });
          } else {
            setUsers({
              ...users,
              [username]: ["FETCHED" as FetchStatus, user],
            });
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
  }, [username]);

  const handleEnter = (ev: any) => {
    if (ev.key === "Enter" && user) {
      router.push(`/${username}`);
    }
  };

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta property="og:title" content="strike.cash" key="title" />
        <meta property="og:description" content={title} key="description" />
        <meta name="theme-color" content={themeColor} />
      </Head>
      <Layout>
        <Flex
          alignItems="center"
          justifyContent="space-between"
          flexDirection={["column", "column", "column", "column", "row"]}
          position="relative"
          width="100%"
        >
          <Nav loggedInUser={loggedInUser} showLogin />
          <Flex
            flexDirection="column"
            alignItems="flex-start"
            width={[300, 420]}
            mt={12}
          >
            <Heading fontSize={["7xl", "8xl"]} mt={[2, 6]} lineHeight={0.9}>
              <FormattedMessage
                {...messages.home_title}
                values={{ strikeLink }}
              />
            </Heading>
            <Text fontSize={["2xl", "4xl"]} mb={2} mt={4}>
              <FormattedMessage {...messages.home_enter_username} />
            </Text>
            <FormControl>
              <InputGroup size="lg" onKeyPress={handleEnter}>
                <Input
                  type="text"
                  value={username}
                  pr="4.5rem"
                  onChange={(ev: any) => setUsername(ev.target.value)}
                />
                <InputRightElement>
                  <Button
                    disabled={!user}
                    colorScheme="orange"
                    w="40px"
                    onClick={() => router.push(`/${user.handle}`)}
                    isLoading={isFetchingUser && !isFetchError}
                    type="button"
                  >
                    <ArrowForwardIcon boxSize={8} />
                  </Button>
                </InputRightElement>
              </InputGroup>
              <FormHelperText>
                <FormattedMessage {...messages.home_go_button} />
              </FormHelperText>
            </FormControl>
          </Flex>
          <CashRegisterPreview user={user} mt={[8, 20]} />
        </Flex>
        {total && <Statistics total={total} />}
        <Footer />
      </Layout>
    </>
  );
};

export default Home;

export const getServerSideProps: GetServerSideProps = async (context) => {
  try {
    const total = null; //await getTotalPaidByCurrency();

    const session = await getSession(context);
    // @ts-expect-error
    const id = session?.user?.id;
    if (id) {
      const loggedInUser = await getProfileById(id);
      return { props: { total, loggedInUser } };
    }

    return {
      props: { total },
    };
  } catch (error) {
    console.error(error);
    return {
      props: {},
    };
  }
};

import type { NextApiRequest, NextApiResponse } from "next";

import { createInvoice, getQuote } from "lib/invoices";

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
  const { amount, handle, description } = req.body;

  if (req.method === "POST") {
    const invoice = await createInvoice({
      handle,
      amount: amount.amount,
      currency: amount.currency,
      description,
    });
    const quote = await getQuote(invoice.shortId);
    res.status(200).json({ ...invoice, ...quote });
  } else {
    res.status(401);
  }
  res.end();
};

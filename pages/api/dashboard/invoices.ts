import type { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/react";

import { getProfileById } from "lib/api";
import { getPaidInvoices } from "lib/db";

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
  // todo: get user from session, 401 if no session
  const { page } = req.query;
  try {
    const session = await getSession({ req });
    // @ts-expect-error
    const id = session?.user?.id;
    if (!session || !id) {
      throw new Error("Unauthorized");
    }
    const user = await getProfileById(id);
    const invoices = await getPaidInvoices(user.handle, {
      page: Number(page) || 1,
    });
    res.status(200).json({ ...invoices });
  } catch (error) {
    console.error(error);
    res.status(401);
  } finally {
    res.end();
  }
};

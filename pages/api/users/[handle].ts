import type { NextApiRequest, NextApiResponse } from "next";

import { getProfileByHandle } from "lib/api";

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
  const { handle } = req.query;

  try {
    if (req.method !== "GET") {
      throw new Error("Method not supported");
    }

    const profile = await getProfileByHandle(handle);
    if (profile) {
      res.status(200).json(profile);
    } else {
      res.status(404).json({ status: "ERROR", reason: "User not found" });
    }
  } catch (error) {
    if (error instanceof Error) {
      res.status(405).json({ status: "ERROR", reason: error.message });
    }
  } finally {
    res.end();
  }
};

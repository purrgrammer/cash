import type { NextApiRequest, NextApiResponse } from "next";

import { getInvoice, getQuote } from "lib/invoices";

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
  const { id } = req.query;

  if (req.method === "GET") {
    const invoice = await getInvoice(id);
    if (invoice) {
      res.status(200).json(invoice);
    } else {
      res.status(404).json({ error: "Invoice not found" });
    }
  } else if (req.method === "POST") {
    const quote = await getQuote(id);
    res.status(200).json(quote);
  } else {
    res.status(405).json({ error: "Method not allowed, only GET and POST" });
  }
  res.end();
};

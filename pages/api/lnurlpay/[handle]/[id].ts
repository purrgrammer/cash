import type { NextApiRequest, NextApiResponse } from "next";

import { getProfileByHandle, getBtcRateInSats } from "lib/api";
import { generateShortId } from "lib/id";
import { createInvoice, getQuote } from "lib/invoices";
import redis from "lib/redis";
import { sha256 } from "lib/hash";

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
  const host = req.headers.host;
  const { handle, id, amount, comment } = req.query;
  const userComment = comment?.slice(0, 250);

  try {
    if (!amount || isNaN(parseInt(String(amount)))) {
      throw new Error("Invalid amount");
    }

    const profile = await getProfileByHandle(handle);
    if (!profile?.canReceive) {
      throw new Error("User not found");
    }

    const lnurlRequest = await redis.get(`lnurlpay:${id}`);
    if (!lnurlRequest) {
      throw new Error("Invalid URL, get another invoice");
    }

    const amountToSend = parseInt(String(amount));
    if (
      lnurlRequest.minSendable > amount ||
      amount > lnurlRequest.maxSendable
    ) {
      throw new Error(
        `Invalid amount, must be between ${lnurlRequest.minSendable} and ${lnurlRequest.maxSendable} milisatoshis`
      );
    }

    const metadata = JSON.parse(lnurlRequest.metadata);
    const description = metadata[0] && metadata[0][1];

    const amountInBtc = amountToSend / 1000 / 1e8;
    const invoiceData = {
      handle,
      amount: amountInBtc.toFixed(8),
      currency: "BTC",
      description: userComment || description,
    };
    const invoice = await createInvoice(invoiceData);
    const descriptionHash = sha256(lnurlRequest.metadata);
    const quote = await getQuote(invoice.shortId, descriptionHash);
    res.status(200).json({
      pr: quote.ln,
      routes: [],
    });
  } catch (error) {
    if (error instanceof Error) {
      res.status(405).json({ status: "ERROR", reason: error.message });
    }
  } finally {
    res.end();
  }
};

import type { NextApiRequest, NextApiResponse } from "next";

import axios from "axios";

import { getProfileByHandle, getBtcRateInSats } from "lib/api";
import { generateShortId } from "lib/id";
import redis from "lib/redis";

export default async (req: NextApiRequest, res: NextApiResponse<any>) => {
  const host = req.headers.host;
  const { handle } = req.query;

  try {
    if (req.method !== "GET") {
      throw new Error("Method not supported");
    }
    const profile = await getProfileByHandle(handle);
    if (!profile?.canReceive) {
      throw new Error("User not found");
    }
    const id = generateShortId();

    const metadata = [
      ["text/plain", `Pay ${handle}`],
      ["text/identifier", `${handle}@${host}`],
    ];

    if (profile.avatarUrl) {
      const avatar = await axios
        .get(profile.avatarUrl, { responseType: "arraybuffer" })
        .then((response) =>
          Buffer.from(response.data, "binary").toString("base64")
        );
      metadata.push(["image/png;base64", avatar]);
    }

    const currency = profile.currencies.find(
      ({ isDefaultCurrency }: any) => isDefaultCurrency
    ).currency;
    const exchangeRate = await getBtcRateInSats(currency);
    const minSendable = Math.ceil(0.01 * exchangeRate) * 1e3;
    const maxSendable = 1e10;
    const lnurlResponse = {
      callback: `https://${host}/api/lnurlpay/${handle}/${id}`,
      minSendable,
      maxSendable,
      metadata: JSON.stringify(metadata),
      commentAllowed: 250,
      tag: "payRequest",
    };
    await redis.set(`lnurlpay:${id}`, lnurlResponse, 600);
    res.status(200).json(lnurlResponse);
  } catch (error) {
    if (error instanceof Error) {
      res.status(405).json({ status: "ERROR", reason: error.message });
    }
  } finally {
    res.end();
  }

  res.end();
};

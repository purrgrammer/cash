import { Icon } from "@chakra-ui/react";

export const DollarIcon = (props: any) => {
  return (
    <Icon viewBox="0 0 24 24" {...props}>
      <g
        fill="none"
        fillRule="evenodd"
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      >
        <path d="M12 .75C18.213.75 23.25 5.787 23.25 12S18.213 23.25 12 23.25.75 18.213.75 12 5.787.75 12 .75z" />
        <path d="M9.75 14.25A2.25 2.25 0 1012 12a2.25 2.25 0 112.25-2.25M12 6v1.5m0 9V18" />
      </g>
    </Icon>
  );
};

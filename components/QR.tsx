import type { FC } from "react";

import { Box } from "@chakra-ui/react";

import QR from "qrcode.react";

export const QRCode = ({ badge, size = 300, ...rest }: any) => {
  const qrSize = size - 20;
  const imgSize = qrSize / 4;
  const imgOffset = qrSize / 2 - imgSize / 2;

  const imageSettings = badge
    ? {
        src: badge,
        x: imgOffset,
        y: imgOffset,
        height: imgSize,
        width: imgSize,
      }
    : {};

  return (
    <Box sx={{ border: "8px solid #fff" }}>
      <QR
        renderAs="canvas"
        imageSettings={imageSettings}
        size={qrSize}
        {...rest}
      />
    </Box>
  );
};

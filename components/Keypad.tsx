import {
  useBreakpointValue,
  Center,
  Button,
  ButtonGroup,
  SimpleGrid,
} from "@chakra-ui/react";

export type Keys = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 0 | "SEP" | "DEL";

interface KeyProps {
  id: Keys;
  width: number;
  height: number;
  value: any;
  onKeyPress(k: Keys): void;
}

const Key = ({ value, id, onKeyPress, width, height }: KeyProps) => {
  return (
    <Button
      fontSize="32px"
      height={height}
      variant="ghost"
      size="lg"
      onClick={() => onKeyPress(id)}
      width={width}
      sx={{ textAlign: "center" }}
    >
      {value}
    </Button>
  );
};

interface KeyPadProps {
  sizes: number[];
  onKeyPress(k: Keys): void;
}

export const KeyPad = ({ sizes, onKeyPress }: KeyPadProps) => {
  const size = useBreakpointValue(sizes) || 300;
  const height = Math.floor(size / 4);
  const width = Math.floor(size / 3);
  return (
    <ButtonGroup width={size} mt={4}>
      <SimpleGrid columns={3} spacing={2} width={size}>
        <Key
          onKeyPress={onKeyPress}
          id={1}
          value={1}
          height={height}
          width={width}
        />
        <Key
          onKeyPress={onKeyPress}
          id={2}
          value={2}
          height={height}
          width={width}
        />
        <Key
          onKeyPress={onKeyPress}
          id={3}
          value={3}
          height={height}
          width={width}
        />

        <Key
          onKeyPress={onKeyPress}
          id={4}
          value={4}
          height={height}
          width={width}
        />
        <Key
          onKeyPress={onKeyPress}
          id={5}
          value={5}
          height={height}
          width={width}
        />
        <Key
          onKeyPress={onKeyPress}
          id={6}
          value={6}
          height={height}
          width={width}
        />

        <Key
          onKeyPress={onKeyPress}
          id={7}
          value={7}
          height={height}
          width={width}
        />
        <Key
          onKeyPress={onKeyPress}
          id={8}
          value={8}
          height={height}
          width={width}
        />
        <Key
          onKeyPress={onKeyPress}
          id={9}
          value={9}
          height={height}
          width={width}
        />

        <Key
          onKeyPress={onKeyPress}
          id={"SEP"}
          value={"."}
          height={height}
          width={width}
        />
        <Key
          onKeyPress={onKeyPress}
          id={0}
          value={0}
          height={height}
          width={width}
        />
        <Key
          onKeyPress={onKeyPress}
          id={"DEL"}
          value={"DEL"}
          height={height}
          width={width}
        />
      </SimpleGrid>
    </ButtonGroup>
  );
};

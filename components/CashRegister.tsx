import { useBreakpointValue, Heading, Box, Center } from "@chakra-ui/react";

import { KeyPad } from "./Keypad";
import type { Keys } from "./Keypad";

const SEP = ".";

export const AmountInput = ({
  amount,
  currency,
}: {
  amount: string;
  currency: string;
}) => {
  const minimumFractionDigits = amount.includes(SEP)
    ? amount.substring(amount.indexOf(SEP)).length - 1
    : 0;

  const intl = new Intl.NumberFormat("en", {
    style: "currency",
    currency: currency === "USDT" ? "USD" : currency,
    minimumFractionDigits: Math.min(2, minimumFractionDigits),
  });

  const xxlFont = "8xl";
  const xlFont = useBreakpointValue(["7xl", "8xl"]);
  const largeFont = useBreakpointValue(["6xl", "7xl"]);

  const fontSize = (() => {
    const length = amount.replace(".", "").length;
    if (length < 3) {
      return xxlFont;
    } else if (length <= 5) {
      return xlFont;
    } else {
      return largeFont;
    }
  })();

  return (
    <Center height={130}>
      <Heading fontSize={fontSize} mb={2}>
        {intl.format(Number(amount))}
        {amount.slice(-1) === SEP && SEP}
      </Heading>
    </Center>
  );
};

interface CashRegisterProps {
  amount: string;
  onChange(n: string): void;
  sizes: number[];
}

export const CashRegister = ({
  amount,
  onChange,
  sizes,
}: CashRegisterProps) => {
  const onKeyPress = (k: Keys) => {
    if (amount.length >= 6 && k !== "DEL") {
      return;
    }

    const hasSeparator = amount.includes(SEP);
    const hasTwoFractionDigits =
      hasSeparator && amount.substring(amount.indexOf(SEP)).length >= 3;
    const isNoOp = amount === "0" && k === 0;

    if (k === "SEP" && !hasSeparator) {
      onChange(amount + SEP);
    } else if (k === "DEL") {
      const newAmount = amount.slice(0, -1);
      onChange(newAmount || "0");
    } else if (!hasTwoFractionDigits && !isNoOp && k !== "SEP") {
      onChange(amount === "0" ? String(k) : amount + k);
    }
  };

  return <KeyPad onKeyPress={onKeyPress} sizes={sizes} />;
};

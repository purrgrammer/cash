import {
  useColorMode,
  useBreakpointValue,
  Box,
  Flex,
  Button,
  Text,
  Avatar,
  Link,
} from "@chakra-ui/react";
import { ExternalLinkIcon, MoonIcon, SunIcon } from "@chakra-ui/icons";
import { signIn, signOut } from "next-auth/react";
import { FormattedMessage } from "react-intl";

import messages from "lib/messages";

export const Nav = ({
  children,
  loggedInUser,
  showLogin = false,
  showLogout = false,
}: any) => {
  const { colorMode, toggleColorMode } = useColorMode();
  const showLeftNav = useBreakpointValue([false, true]);
  const isLoginEnabled = false;

  return (
    <Box as="nav">
      <Box sx={{ position: "absolute", top: 2, left: 2 }}>
        {isLoginEnabled && showLogin && !loggedInUser && (
          <Button
            variant="outline"
            colorScheme="teal"
            onClick={() => signIn("strike")}
          >
            <FormattedMessage {...messages.log_in} />
          </Button>
        )}
        {isLoginEnabled && showLogin && loggedInUser && (
          <Flex alignItems="center" flexDirection="row" mt={2}>
            <Avatar
              name={loggedInUser.handle}
              src={loggedInUser.avatarUrl}
              mr=".5em"
              size="xs"
            />
            <Text fontWeight="bold">
              <Link href={`/dashboard`}>{loggedInUser.handle}</Link>
            </Text>
          </Flex>
        )}
        {showLeftNav && children}
      </Box>
      <Box sx={{ position: "absolute", top: 2, right: 2 }}>
        {isLoginEnabled && showLogout && (
          <Button colorScheme="red" variant="outline" onClick={() => signOut()}>
            <FormattedMessage {...messages.log_out} />
          </Button>
        )}
        <Button variant="unstyled" onClick={toggleColorMode}>
          {colorMode === "light" ? (
            <MoonIcon color="gray.300" />
          ) : (
            <SunIcon color="yellow.300" />
          )}
        </Button>
      </Box>
    </Box>
  );
};

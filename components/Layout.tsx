import { Flex } from "@chakra-ui/react";

export const Layout = ({ children, ...rest }: any) => {
  return (
    <Flex
      alignItems="center"
      justifyContent="space-between"
      flexDirection="column"
      mx="auto"
      maxW="1000px"
      position="relative"
      {...rest}
    >
      {children}
    </Flex>
  );
};

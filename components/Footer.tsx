import { Center, Text, Link } from "@chakra-ui/react";
import { ExternalLinkIcon } from "@chakra-ui/icons";

import { FormattedMessage } from "react-intl";

import messages from "lib/messages";

export const Footer = () => {
  const author = (
    <Link color="orange" href="https://strike.cash/qr/verbiricha">
      verbiricha
    </Link>
  );
  return (
    <Center as="footer" mt={10} mb={5}>
      <Text color="gray.500">
        <FormattedMessage {...messages.footer} values={{ author }} />
      </Text>
    </Center>
  );
};
